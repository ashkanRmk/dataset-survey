import React, {Component} from 'react';
import '../../dist/css/index.css';
import 'antd/dist/antd.css';
import {Card, Row, Col, notification, Icon} from 'antd';
import Drawer from '../Drawer/drawer';
import {connect} from 'react-redux';

const {Meta} = Card;

const gridStyle = {
  // width: '314px',
  // height: '314px',
  textAlign: 'center',
  // paddingTop: '120px',
};

const openNotification = () => {
  notification.open ({
    message: 'عنوان تستی',
    description: 'متن توضیحات تسیتی',
    icon: <Icon type="smile" style={{color: '#108ee9'}} />,
  });
};

class Cards extends Component {
  constructor (props) {
    super (props);
    this.state = {
      loading: false,
      visible: false,

      visibleDrawer: false,
      visibleChildDrawer: false,
      placement: 'bottom',
      podcastList: null,
    };

    this.closeDrawer = this.closeDrawer.bind (this);
  }

  componentDidMount () {}

  notif (type) {
    notification.open ({
      message: type,
      description: 'متن توضیحات تسیتی',
      icon: <Icon type="smile" style={{color: '#108ee9'}} />,
    });
  }

  showDrawer (sub) {
    let podcastList = this.props.podcasts.filter (x => {
      return x.main_subject === sub;
    });
    this.setState ({podcastList});
    this.setState ({visibleDrawer: true, sub});
  }

  closeDrawer () {
    this.setState ({visibleDrawer: false});
  }

  render () {
    let subjects = this.props.subjects;
    let res = null;
    if (subjects) {
      res = [...subjects];
    }

    return (
      <div
      >
				<h6 style={{fontSize: 30,
          color: '#555',
				}}>
          لطفا یکی از موضوعات زیر که پادکست‌های مرتبط با آن را شنیده‌اید انتخاب کنید
        </h6>
        <br />

        <Drawer
          podList={this.state.podcastList}
          sub={this.state.sub}
          visibleDrawer={this.state.visibleDrawer}
          closeDrawer={this.closeDrawer}
          visible={this.state.visible}
          closeModal={this.closeModal}
        />
        <Row type="flex" justify="center" gutter={18}>
          {res &&
            res.map (subject => {
              return (
                <Col
                  className="gutter-row"
                  xs={12}
                  sm={6}
									style={{paddingBottom: '15px'	}}
                >
                  <Card
                    hoverable
                    style={gridStyle}
                    onClick={this.showDrawer.bind (this, subject)}
                  >
                    <Meta title={subject} />
                  </Card>
                </Col>
              );
            })}
        </Row>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    // ...store,
    podcasts: store.Podcasts.podcastsList,
  };
};

export default connect (mapStateToProps) (Cards);
