import React, { Component } from 'react';
import '../../dist/css/index.css';
import 'antd/dist/antd.css';
import { Button, Drawer, Col, Row, Rate, Icon } from 'antd';
import { connect } from 'react-redux';
import Modal from '../Modals/modal';

class MultiDrawer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleChildDrawer: false,
			placement: 'bottom',
			value: 0,
			titles: [],
			childDesc: null,
			subSubject: null,
		};
		this.closeModal = this.closeModal.bind(this);
		this.showChildrenDrawer = this.showChildrenDrawer.bind(this);
	}
	componentDidMount() {}

	showChildrenDrawer(item) {
		this.setState({
			visibleChildDrawer: true,
			podcast: item,
			childDesc: item.desc,
			subSubject: item.sub_subjects,
		});
	}

	onChildrenDrawerClose = () => {
		this.setState({
			visibleChildDrawer: false,
			value: 0,
		});
	};

	onClose = () => {
		this.setState({ value: 0 });
		this.props.closeDrawer();
	};

	onChange = e => {
		this.setState({
			placement: e.target.value,
		});
	};

	handleChange = value => {
		this.setState({ value });
	};

	showModal(id) {
		this.setState({
			visible: true,
		});
	}

	closeModal() {
		this.setState({
			visible: false,
		});
	}
	render() {
		const { value } = this.state;
		let podList = this.props.podList;

		return (
			<div
>				<Modal
					visible={this.state.visible}
					closeModal={this.closeModal}
					closeChildDrawer={this.onChildrenDrawerClose}
					subSubject={this.state.subSubject}
					podcast={this.state.podcast}
					mainSubjectRating={value}
				/>

				<Drawer
					title="لطفا از لیست زیر یک پادکستی که قبلا شنیدید را انتخاب کنید"
					placement={this.state.placement}
					closable={false}
					style={{ overflow: 'scroll' }}
					onClose={this.onClose}
					visible={this.props.visibleDrawer}>
					<div className="btn-geek" style={{ overflow: 'auto' }}>
						<div style={{ overflowY: 'auto', display: 'flex', flexWrap: 'wrap', flexDirection: 'row-reverse' }}>
							{podList &&
								podList.map(item => {
									let hasRated = this.props.ratings.byPodcastId[item.id] ? true : false;
									return (
										<Button
											onClick={this.showChildrenDrawer.bind(this, item)}
											style={{ marginRight: 25, marginBottom: 25, backgroundColor: hasRated ? '#1EDD62' : '#F0F5FF' }}>
											{item.name}
										</Button>
									);
								})}
						</div>
					</div>
					<Drawer
						title="به صورت کلی چه امتیازی به این پادکست می‌دهید؟"
						placement={this.state.placement}
						closable={false}
						onClose={this.onChildrenDrawerClose}
						visible={this.state.visibleChildDrawer}>
						<div style={{ overflow: 'auto' }}>
							<span>
								<p style={{ textAlign: 'center' }}>{this.state.childDesc}</p>
								<Rate onChange={this.handleChange} value={value} style={{ height: 70 }} />
								{<span className="ant-rate-text" />}
							</span>
							<div style={{ display: 'flex', width: '100%', flexDirection: 'column', alignItems: 'center' }}>
								<Button onClick={this.showModal.bind(this)}>تایید</Button>
							</div>
						</div>

						<br />
					</Drawer>
				</Drawer>
			</div>
		);
	}
}

const mapStateToProps = store => ({
	ratings: store.Podcasts.ratings,
});

export default connect(mapStateToProps)(MultiDrawer);
