import * as ActionTypes from '../_constant/ActionType';

const initialState = {
	podcastsList: [],
	age: null,
	gender: null,
	ratings: {
		byPodcastId: {}
	}
}

export default function moduleReducer(
	state = initialState,
	action
) {
	switch (action.type) {
		case ActionTypes.GET_PODCASTS_SUCCESS: {
			return {
				...state,
				type: action.type,
				podcastsList: action.payload.podcasts,
			};
		}
		case "SAVE_RATING": {
			let { podcast, mainSubjectRating, subRatings } = action.payload
			return {
				...state,
				ratings: {
					...state.ratings,
					byPodcastId: {
						...state.ratings.byPodcastId,
						[podcast.id]: {
							podcast,
							main_subject_rate: mainSubjectRating,
							sub_ratings: subRatings
						}
					}
				}
			}
		}

		case "SET_GENDER": {
			let { gender } = action.payload
			return {
				...state,
				gender
			}
		}

		case "SET_AGE": {
			let { age } = action.payload
			return {
				...state,
				age
			}
		}
		default:
			return state;
	}
}
