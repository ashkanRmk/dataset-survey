import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import '../../dist/css/index.css';
import 'antd/dist/antd.css';
import { Button, Modal, Icon, message } from 'antd';
import Cards from '../Cards/card.js';
import { connect } from 'react-redux';
import { submitToServer } from '../_actions';
import disableBrowserBackButton from 'disable-browser-back-navigation';

class Survey extends Component {
	state = {
		loading: false,
		visible: false,
		redirect: false,
		subjects: null,
	};

	componentDidMount() {
		let res1 = this.props.podcasts.map(x => {
			return x.main_subject;
		});
		let subjects = new Set(res1);
		this.setState({ subjects });

		disableBrowserBackButton();
	}

	handleOk = async () => {
		this.setState({ loading: true });
		try {
			let a = await this.props.dispatch(submitToServer());
			this.setState({ loading: false, redirect: true, visible: false });
		} catch (err) {
			console.log(err);
			message.error('مشکلی در پردازش فرم وجود دارد');
			this.setState({ submitError: false, loading: false, redirect: false });
		}

		// setTimeout(() => {
		// 	this.setState({ loading: false, redirect: true, visible: false });
		// }, 3000);
	};

	handleCancel() {
		this.setState({ visible: false });
	}

	render() {
		const { loading } = this.state;

		return (
			<div className="App back">
				<Cards subjects={this.state.subjects} />

				<br />
				<Button
					onClick={() => {
						this.setState({ visible: true });
					}}
					type="primary"
					size="large"
					style={{
						color: '#2f54eb',
						background: '#f0f5ff',
						borderColor: '#adc6ff',
					}}>
					<Icon type="select" />
					ارسال نهایی
				</Button>

				<br />
				<br />
				<Link to="/">
					<Button type="primary">
						<Icon type="double-left" />
					</Button>
				</Link>

				<Modal
					visible={this.state.visible}
					title="کاملا مطمئنی؟"
					onOk={this.handleOk}
					onCancel={this.handleCancel.bind(this)}
					style={{ textAlign: 'right' }}
					footer={[
						<Button key="back" onClick={this.handleCancel.bind(this)}>
							تکمیل امتیازها
						</Button>,
						Object.keys(this.props.ratings.byPodcastId).length ? (
							<Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
								ارسال نهایی
							</Button>
						) : null,
					]}>
					{Object.keys(this.props.ratings.byPodcastId).length ? (
						<>
							<p>پادکست هایی ک بهشون امتیاز دادی </p>
							<div style={{ overflowY: 'auto', display: 'flex', flexWrap: 'wrap', flexDirection: 'row-reverse' }}>
								{Object.keys(this.props.ratings.byPodcastId).map(podcastId => {
									let { podcast } = this.props.ratings.byPodcastId[podcastId];
									return (
										<Button style={{ marginRight: 15, marginBottom: 25, backgroundColor: '#1EDD62' }}>
											{podcast.name}
										</Button>
									);
								})}
							</div>
						</>
					) : (
						<p>هنوز به هیچ پادکستی امتیاز ندادی.</p>
					)}
				</Modal>
				{this.state.redirect && <Redirect to="/thanks" />}
			</div>
		);
	}
}

const mapStateToProps = store => {
	return {
		podcasts: store.Podcasts.podcastsList,
		ratings: store.Podcasts.ratings,
	};
};

export default connect(mapStateToProps)(Survey);
