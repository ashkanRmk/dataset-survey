import React, { Component } from 'react';
import '../../dist/css/index.css';
import 'antd/dist/antd.css';
import { Modal, Button, Icon, Slider } from 'antd';
import { connect } from 'react-redux';

import { saveRating } from '../_actions/index';

class Modals extends Component {
	state = {
		loading: false,
		values: {},
		subjects: [],
		value: 0,
	};
	componentDidMount() {}

	handleOk = async () => {
		let { podcast, mainSubjectRating } = this.props;
		let { values } = this.state;
		this.props.dispatch(saveRating({ podcast, mainSubjectRating, values }));
		await this.setState({ values: {} });
		this.props.closeModal();
		this.props.closeChildDrawer();
	};

	handleCancel() {
		this.props.closeModal();
	}

	handleChange = (subSubject, value) => {
		let { values } = this.state;
		this.setState({
			values: {
				...values,
				[subSubject.id]: {
					...subSubject,
					rating: value,
				},
			},
		});
	};

	render() {
		const { loading } = this.state;

		// const { max, min } = this.props;
		const max = 5;
		const min = 1;
		const { value } = this.state;
		const mid = ((max - min) / 2).toFixed(5);
		const preColor = value >= mid ? '' : 'rgba(0, 0, 0, .45)';
		const nextColor = value >= mid ? 'rgba(0, 0, 0, .45)' : '';

		let subSubjects = this.props.subSubject;

		return (
			<div>
				<Modal
					visible={this.props.visible}
					title="ارزیابی"
					onOk={this.handleOk}
					onCancel={this.handleCancel.bind(this)}
					footer={[
						<Button key="back" onClick={this.handleCancel.bind(this)}>
							بازگشت
						</Button>,
						<Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
							ثبت
						</Button>,
					]}>
					{subSubjects &&
						subSubjects.map(item => {
							return (
								<div style={{display: 'flex' }}>
									<div className="icon-wrapper" style={{ flex: 1, marginRight: 20 }}>
										<Icon style={{ color: preColor }} type="frown-o" />
										<Slider
											max={5}
											min={1}
											onChange={value => this.handleChange(item, value)}
											value={(this.state.values[item.id] && this.state.values[item.id].rating) || 0}
										/>
										<Icon style={{ color: nextColor }} type="smile-o" />
									</div>
									<p style={{ flex: 0.1 }}>{item.name}</p>
								</div>
							);
						})}
				</Modal>
			</div>
		);
	}
}

export default connect()(Modals);
