import { combineReducers } from 'redux';
import Podcasts from '../components/_reducer';

const appReducer = combineReducers({
	Podcasts,
});

//For Clearing States After Logout
export default (state, action) => {
	// if (action.type === 'LOGOUT')
	//   state = undefined;
	// }

	return appReducer(state, action);
};
